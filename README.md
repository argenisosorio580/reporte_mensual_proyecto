<h1>Sistema para reportar mensualmente los avances de un proyecto</h1>

<b>Creado por Argenis Osorio en la Fundación CENDITEL</b>

<hr />

## Paquetes requeridos
```
Python==2.7
Django==1.8.8
```

## Instalación del entorno de desarrollo

Usaremos # para los comandos como superusuario

Usaremos $ para los comandos como usuario regular

```

$ cd reporte_mensual_proyecto

$ cp settings.py_example settings.py

$ cd ..

$ bash reset_db.sh

$ python manage.py runserver
```

## Capturas

![captura-1.jpg](captura-1.jpg "captura-1.jpg")
