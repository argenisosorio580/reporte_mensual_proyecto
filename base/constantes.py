# -*- coding: utf-8 -*-

meses = (
    ('-', '-'),
    ('Enero','Enero'),
    ('Febrero','Febrero'),
    ('Marzo','Marzo'),
    ('Abril','Abril'),
    ('Mayo','Mayo'),
    ('Junio','Junio'),
    ('Julio','Julio'),
    ('Agosto','Agosto'),
    ('Septiembre','Septiembre'),
    ('Octubre','Octubre'),
    ('Noviembre','Noviembre'),
    ('Diciembre','Diciembre'),
)
anos = (
    ('-', '-'),
    ('2018','2018'),
    ('2019','2019'),
)
cargos = (
    ('-', '-'),
    ('Analista de Desarrollo','Analista de Desarrollo'),
    ('Analista de Apropiación','Analista de Apropiación'),
    ('Analista de Reflexión','Analista de Reflexión'),
    ('Analista de Investigación','Analista de Investigación'),
    ('Analista de Administración','Analista de Administración'),
)
directores = (
    ('-', '-'),
    ('Cipriano Alvarado','Cipriano Alvarado'),
    ('Argenis Osorio','Argenis Osorio'),
    ('Julie Vera','Julie Vera'),
    ('Santiago Roca','Santiago Roca'),
    ('Adriana Fandiño','Adriana Fandiño'),
)